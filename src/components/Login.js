import React, { useState, useEffect} from "react"
import { navigate } from "gatsby"
import Form from "./Form"
import View from "./View"
import { handleLogin, isLoggedIn } from "../utils/auth"

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleUpdate = (event) => {
    if (event.target.name === 'username') setUsername(event.target.value);
    if (event.target.name === 'password') setPassword(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault()
    handleLogin({ username, password })
  }

  useEffect(() => {
    if (isLoggedIn()) {
      navigate(`/app/profile`)
    }
  })

  return (
    <View title="Log In">
      <Form
        handleUpdate={e => handleUpdate(e)}
        handleSubmit={e => handleSubmit(e)}
      />
    </View>
  )
}

export default Login
