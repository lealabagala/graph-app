import React from "react"
import { Link } from "gatsby"
import Layout from "../components/Layout"
import View from "../components/View"
import Status from "../components/Status"
import LineGraph from "../components/LineGraph"
import { isLoggedIn } from "../utils/auth"

const Index = () => {
  return (
    <Layout>
      <Status />
      <View title="Simple Auth and Graphing Example">
        { isLoggedIn() ? 
          <LineGraph /> :
          <React.Fragment>
            <p>
              This is a simple example of creating dynamic apps with Gatsby that
              require user authentication.
            </p>
            <p>
              To view the line graph, kindly
              {` `}
              <Link to="/app/login">login</Link>.
            </p>
          </React.Fragment> }
      </View>
    </Layout>
  )
}

export default Index
